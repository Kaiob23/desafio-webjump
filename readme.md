# Desafio Webjump

Kaio Barbosa <kaiob690@gmail.com>

---

Projeto desenvolvido para fins de avaliação técnica no processo seletivo da Webjump.

## Tecnologias utilizadas

- Servidor Unix/Linux
- PHP 7.4
- MariaDB 10.3 
- Docker-CE & Docker-Compose
- Composer

## Bibliotecas externas

- Twig ^3.3.3
- Illuminate/Database 6.14.0
- Monolog ^2.3.4
- Rakit/Validation ^1.4

## Arquitetura e padrões de projeto
- MVC
- Repository
- PSR4
- Singleton

# Executando o projeto
Obs: Tenha o docker, docker compose e composer instalados.

- Clone o repositório e acesse a pasta do repositório clonado.
- Altere para o branch **"desafio"** ```git checkout desafio```.
- Faça uma cópia do arquivo **"sample.env"** e o renomeie para apenas .env, este arquivo contém as configurações de portas e credencias do projeto
- Certifique-se que as portas configuradas no arquivo .env estão livres.
- Realize o build dos containers: ```docker-compose up -d --build``` 
- Certifique que eles foram iniciados: ```docker-compose start```
- Acesse o banco de dados via phpmyadmin em http://localhost:8080 e certifique-se de que o banco **"db-teste-webjump"** foi criado
- Acesse o container: ```docker exec -it webserver-teste-webjump bash``` e execute a instalação do composer: ```composer install```
- Acesse http://localhost para visualizar o desafio.
- Para parar a execução dos container, execute o comando: ```docker-compose stop```